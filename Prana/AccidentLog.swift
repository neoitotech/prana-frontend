//
//  AccidentLog.swift
//  Prana
//
//  Created by Thahir Maheen on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import Foundation

class AccidentLog {
    
    var id = 0
    var comments = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    class func reportAccident(latitude: Double, longitude: Double, comments: String) {
        
        let params = ["latitude": "\(latitude)", "longitude": "\(longitude)", "comments": comments]
        
        ParseEngine.sharedEngine.postData(keyPath: "event/\(Model.sharedModel.deviceToken)", params: params) { (data, error) -> () in
            //
        }
    }
}