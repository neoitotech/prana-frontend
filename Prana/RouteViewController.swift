//
//  RouteViewController.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {
    
    var coordinate = CLLocationCoordinate2D()
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}

class RouteViewController: UIViewController {

    var startLat: Double = 8.557
    var startLng: Double = 76.882
    var endLat: Double = 8.551
    var endLng: Double = 76.871
    
    @IBOutlet weak var mapView: MKMapView!
    
    var routeLine: MKPolyline?
    var routeLineView: MKPolylineView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadMap()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadMap() {
        
        var coordinates: [CLLocationCoordinate2D] = []
        coordinates.append(CLLocationCoordinate2DMake(startLat, startLng))
        coordinates.append(CLLocationCoordinate2DMake(endLat, endLng))
        let viewRegion = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(startLat, startLng), 5000, 5000)
        let adjustedRegion = mapView.regionThatFits(viewRegion)
        mapView.setRegion(adjustedRegion, animated: true)
        
        var polyLine = MKPolyline(coordinates: &coordinates, count: coordinates.count)
        self.mapView.addOverlay(polyLine, level: MKOverlayLevel.AboveRoads)
        
        mapView.addAnnotation(Annotation(coordinate: CLLocationCoordinate2DMake(startLat, startLng)))
        mapView.addAnnotation(Annotation(coordinate: CLLocationCoordinate2DMake(endLat, endLng)))
    }

    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay.isKindOfClass(MKPolyline) {
            // draw the track
            let polyLine = overlay
            let polyLineRenderer = MKPolylineRenderer(overlay: polyLine)
            polyLineRenderer.strokeColor = UIColor.blueColor()
            polyLineRenderer.lineWidth = 2.0
            
            return polyLineRenderer
        }
        
        return nil
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
