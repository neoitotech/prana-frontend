//
//  SendBloodDonorRequestTableViewController.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class SendBloodDonorRequestTableViewController: UITableViewController {
    
    enum PickerMode: String {
        case Location = "Location"
        case Bloodgroup = "Bloodgroup"
    }
    
    var pickerMode = PickerMode.Location
    var currentTextField: UITextField?
    
    @IBOutlet var picker: UIPickerView!
    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet weak var textFieldBloodGroup: UITextField!
    @IBOutlet weak var textFieldLocation: UITextField!
    @IBOutlet weak var textViewComments: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldBloodGroup.inputView = picker
        textFieldLocation.inputView = picker
        
        textFieldBloodGroup.inputAccessoryView = toolBar
        textFieldLocation.inputAccessoryView = toolBar
        
        textFieldBloodGroup.becomeFirstResponder()
    }
    
    // picker view delegates and datasource
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerMode {
            
        case .Location:
            return Locations.sharedLocations.locations.count
        case .Bloodgroup:
            return BloodGroup.allGroups.count
        default:
            break
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        switch pickerMode {
            
        case .Location:
            return Locations.sharedLocations.locations[row]
        case .Bloodgroup:
            return BloodGroup.allGroups[row].rawValue
        default:
            break
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        currentTextField?.text = pickerMode == PickerMode.Location ? Locations.sharedLocations.locations[row] : BloodGroup.allGroups[row].rawValue
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        currentTextField = textField
        
        pickerMode = textField == textFieldLocation ? PickerMode.Location : PickerMode.Bloodgroup
        picker.reloadAllComponents()
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func selectActionPicker(sender: UIBarButtonItem) {
        
        currentTextField?.text = pickerMode == PickerMode.Location ? Locations.sharedLocations.locations[picker.selectedRowInComponent(0)] : BloodGroup.allGroups[picker.selectedRowInComponent(0)].rawValue
        
        currentTextField?.resignFirstResponder()
    }

    @IBAction func buttonActionCurrentLocation(sender: UIButton) {
        textFieldLocation.text = "Current Location"
    }
    
    @IBAction func barButtonActionSend(sender: UIBarButtonItem) {
        Model.sharedModel.requestBlood(bloodGroup: textFieldBloodGroup.text, comments: textViewComments.text)
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
}
