//
//  ParseEngine.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import Foundation
import AFNetworking

let baseUrl = "http://prana.azurewebsites.net"
//let baseUrl = "http://local.prana.com"

class ParseEngine: AFHTTPSessionManager {
    
    class var sharedEngine: ParseEngine {
        struct Singleton {
            static let sharedEngine = ParseEngine(baseURL: NSURL(string: baseUrl), sessionConfiguration: NSURLSessionConfiguration.defaultSessionConfiguration())
        }
        return Singleton.sharedEngine
    }
    
    override init(baseURL url: NSURL!, sessionConfiguration configuration: NSURLSessionConfiguration!) {
        super.init(baseURL: url, sessionConfiguration: configuration)
        
        // set serializers
        responseSerializer = AFJSONResponseSerializer()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func fetchData(keyPath: String = "", params: [String: String] = [:], completionHandler: (data: NSDictionary?, error: NSError?) -> ()) {
        
        GET(keyPath, parameters: params, success: { (dataTask, data) in
            
            // handle errors if any
            let dictionaryData = data as! NSDictionary
            if let metaData = dictionaryData["meta"] as? NSDictionary {
                let meta = Meta(data: metaData)
                
                if meta.code != 200 {
                    // configure error
                    let error = NSError(domain: "", code: meta.code, userInfo: nil)
                    dispatch_async(dispatch_get_main_queue()) {
                        completionHandler(data: nil, error: error)
                    }
                    return
                }
            }
            else { println("XXX no meta found, fix api") }
            
            dispatch_async(dispatch_get_main_queue()) {
                completionHandler(data: dictionaryData, error: nil)
            }
            }, failure: { (dataTask, error) in
                
                // customize the error here
                dispatch_async(dispatch_get_main_queue()) {
                    completionHandler (data: nil, error: error)
                }
        })
    }
    
    func fetchArrayData(keyPath: String = "", params: [String: String] = [:], completionHandler: (data: NSArray?, error: NSError?) -> ()) {
        
        GET(keyPath, parameters: params, success: { (dataTask, data) in
            
            dispatch_async(dispatch_get_main_queue()) {
                completionHandler(data: data as? NSArray, error: nil)
            }
            }, failure: { (dataTask, error) in
                
                // customize the error here
                dispatch_async(dispatch_get_main_queue()) {
                    completionHandler (data: nil, error: error)
                }
        })
    }
    
    
    func postData(keyPath: String = "", params: [String: String] = [:], completionHandler:(data: NSDictionary?, error: NSError?) -> ()) {
        
        POST(keyPath, parameters: params, success: { (dataTask, data) in
            
            println("response is \(data)")
            
            // handle errors if any
            let dictionaryData = data as! NSDictionary
            if let metaData = dictionaryData["meta"] as? NSDictionary {
                let meta = Meta(data: metaData)
                
                if meta.code != 200 {
                    // configure error
                    let error = NSError(domain: "", code: meta.code, userInfo: nil)
                    dispatch_async(dispatch_get_main_queue()) {
                        completionHandler(data: nil, error: error)
                    }
                    return
                }
            }
            else { println("XXX no meta found, fix api") }
            dispatch_async(dispatch_get_main_queue()) {
                completionHandler(data: data as? NSDictionary, error: nil)
            }
            
            }, failure: { (dataTask, error) in
                
                // customize the error here
                dispatch_async(dispatch_get_main_queue()) {
                    completionHandler (data: nil, error: error)
                }
        })
    }
}