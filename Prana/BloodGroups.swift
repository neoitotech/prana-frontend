//
//  BloodGroups.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import Foundation

enum BloodGroup: String {
    case APositive = "A+"
    case BPositive = "B+"
    case ABPositive = "AB+"
    case OPositive = "O+"
    case ANegative = "A-"
    case BNegative = "B-"
    case ABNegative = "AB-"
    case ONegative = "O-"
    
    static var allGroups: [BloodGroup] {
        return [.APositive, .BPositive, .ABPositive, .OPositive, .ANegative, .BNegative, .ABNegative, .ONegative];
    }
}