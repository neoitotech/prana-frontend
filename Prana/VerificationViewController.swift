//
//  VerificationViewController.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {

    @IBOutlet weak var textFieldVerificationCode: UITextField!

    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        return "\(Model.sharedModel.currentUser.verificationCode)" == textFieldVerificationCode.text
    }
}
