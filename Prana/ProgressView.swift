//
//  ProgressView.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class ProgressView: UIView {

    var startAngle: Double = 0.0
    var progressCircle = CAShapeLayer()
    var labelCount: UILabel?
    
    var endAngle: CGFloat = 0.0 {
        didSet {
            
            progressCircle.strokeEnd = endAngle
            
            let labelAnimation = CATransition()
            labelAnimation.duration = 0.1
            labelAnimation.type = kCATransitionFade
            labelAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            labelCount?.layer.addAnimation(labelAnimation, forKey: "labelAnimation")
            
            labelCount?.text = "\(Int(endAngle * 10.0))"
        }
    }
    var textContent: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        fatalError("init(coder:) has not been implemented")
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        let circle = UIView()
        labelCount = UILabel()
        labelCount?.textAlignment = NSTextAlignment.Center
        labelCount?.font = UIFont.systemFontOfSize(15.0)//UIFont(name: "GillSans-Light", size: 15.0)
        labelCount?.textColor = UIColor.whiteColor()
        circle.bounds = CGRect(x: 0,y: 0, width: CGRectGetWidth(rect), height: CGRectGetHeight(rect))
        circle.frame = CGRectMake(0,0, CGRectGetWidth(rect), CGRectGetHeight(rect))
        labelCount?.frame = circle.frame
        circle.layoutIfNeeded()
        
        
        let centerPoint = CGPoint (x: circle.bounds.width / 2, y: circle.bounds.width / 2);
        let circleRadius : CGFloat = circle.bounds.height - 30
        
        var circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(-0.5 * M_PI), endAngle: CGFloat(1.5 * M_PI), clockwise: true    )
        
        progressCircle = CAShapeLayer ()
        progressCircle.path = circlePath.CGPath
        progressCircle.strokeColor = UIColor.themeHighlightColor().CGColor
        progressCircle.fillColor = UIColor.themeDarkGrayColor().CGColor
        progressCircle.lineWidth = 10
        progressCircle.strokeStart = 0
        progressCircle.strokeEnd = endAngle
        progressCircle.shadowColor = UIColor.themeLightGrayColor().CGColor
        progressCircle.shadowRadius = 8.0
        progressCircle.shadowOpacity = 0.9
        progressCircle.shadowOffset = CGSize(width: 0, height: 0)
        
        circle.layer.addSublayer(progressCircle)
        labelCount?.text = "\(Int(endAngle * 100.0))" + "%"
        circle.addSubview(labelCount!)
        self.addSubview(circle)
        
    }

}
