//
//  BloodRequest.swift
//  Prana
//
//  Created by Thahir Maheen on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import Foundation

class BloodRequest {
    
    var id = 0
    var bloodGroup = ""
    var comments = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    class func requestBlood(latitude: Double, longitude: Double, bloodGroup: String, comments: String) {
        
        let params = ["latitude": "\(latitude)", "longitude": "\(longitude)", "comments": comments, "blood_group": bloodGroup]
        
        ParseEngine.sharedEngine.postData(keyPath: "blood/\(Model.sharedModel.deviceToken)", params: params) { (data, error) -> () in
            //
        }
    }
}