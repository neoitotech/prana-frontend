//
//  AlertRequest.swift
//  Prana
//
//  Created by mini on 23/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

class AlertRequest {
   
    var date = ""
    var comments = ""
    var type = ""
    
    init() {
        
    }
    
    init(data: NSDictionary) {
        self.date = data["reported_dt"] as? String ?? ""
        self.comments = data["comments"] as? String ?? ""
        self.type = data["type"] as? String ?? ""
    }
    
    class func listAlerts(completionHandler: (alerts: [AlertRequest]) -> ()) {
        
        ParseEngine.sharedEngine.fetchArrayData(keyPath: "alert") { (data, error) in
            var arrayData = [AlertRequest]()
            for dict in data! {
                arrayData.append(AlertRequest(data: dict as! NSDictionary))
            }
            completionHandler(alerts: arrayData)
        }
    }
}

