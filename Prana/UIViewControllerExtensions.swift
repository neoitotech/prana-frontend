//
//  UIViewControllerExtensions.swift
//  Prana
//
//  Created by Thahir Maheen on 22/08/15.
//  Copyright (c) 2015 mini. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var appDelegate: AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
}